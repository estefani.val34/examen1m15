/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *Do the test of ADN_Manager
 * @author tarda
 */
public class DNAManagerTests2 {
    
    String dnaSequence;
    ADN_Manager dnaFunct;
    
    public DNAManagerTests2() {
        dnaSequence = "GATATGC";
        dnaFunct = new ADN_Manager();
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.

    @Test
    public void testMinLetterDNA_COneTime() {
        dnaSequence = "GATATGC";
        String expected = "C";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result,expected);
    }
    
    @Test
    public void testMinLetterDNA_T_Twice() {
        dnaSequence = "CCGATACATGAC";
        String expected = "T";
        String result = dnaFunct.minLetter(dnaSequence);
        assertEquals(result,expected);
    }
    

    //Recompte d’adenines (bases d’A) en una cadena ADN.
    @Test
    public void testvalRecompteA() {
        String test="gtgt";
        TestFalse(test);
    }

    private void TestFalse(String test) {
        int expectedResult = 0;
        int result = ADN_Manager.numAdenines(test);
        System.out.println("");
        assertEquals(expectedResult, result);
    }

  
    @Test
    public void testCorrectRecompteA() {
        String test = "AAA";
        int expectedResult = 3;
        int result = ADN_Manager.numAdenines(test);
        System.out.println("");
        assertEquals(expectedResult, result);
    }
    
    //Percentatge d’adenines (bases d’A) en una cadena ADN.
    @Test
    public void testvalpercentatgeA() {
        double bases=4;
        double a=2;
        TestpercentatgeFalse(bases,a);
    }

    private void TestpercentatgeFalse(double bases, double a) {
        double expectedResult = 0.5*100;
        double result = ADN_Manager.percentatgebase(bases, a);
        System.out.println("");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCorrectpercentatgeRecompteA() {
        double bases=4;
        double a=4;
        double expectedResult = 1*100;
        double result = ADN_Manager.percentatgebase(bases, a);
        System.out.println("");
        assertEquals(expectedResult, result);
    }
    
    //Base més repetida
       @Test
    public void testvalbasemesrepetida() {
        String ADN="AGTA";
        Testbasemesrepetida(ADN);
    }

    private void Testbasemesrepetida(String ADN) {
        String expectedResult = "A";
        String result = ADN_Manager.maxLetter(ADN);
        System.out.println("");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCorrectbasemesrepetida() {
        String ADN="ATT"; 
        String expectedResult = "T";
        String result = ADN_Manager.maxLetter(ADN);
        System.out.println("");
        assertEquals(expectedResult, result);
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import java.util.Scanner;

/**
 *Get the new dna sequence by shell
 * @author tarda
 */
public class ADNReadTeclat {

    /**
     * Read dna sequence by shell
     *
     * @return String dna
     */
    public String adnteclat() {
        Scanner myScan = new Scanner(System.in);
        System.out.println("Enter a new dna sequence:");
        String dna = myScan.nextLine();
        return dna.toUpperCase();
    }

}

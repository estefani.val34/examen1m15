package prsteven.practica_1_2_1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author stevenrolf
 * @version 1.0
 */

public class Validator {

    //@return Se crea funcion boolean para validar el campo si esta vacio o no
    public boolean validarCamp(String camp) {
        if (camp != null && camp.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
//@return Se crea funcion para malidar si el primer caracter es amyuscula o no

    public boolean validarMayuscula(String camp) {
        Pattern name = Pattern.compile("^[A-Z]+[a-z]*");
        Matcher condition = name.matcher(camp);
        if (!condition.find()) {
            return false;
        } else {
            return true;
        }
    }
//@return Se crea funcion para validar el tamaño del string

    public boolean validaSize(String camp) {
        if (camp.length() < 40) {
            return true;
        } else {
            return false;
        }
    }
//@return Se crea funcion para validar el correo con regex

    public boolean validarMail(String camp) {
        Pattern patron = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = patron.matcher(camp);
        if (!mather.find()) {
            return false;
        } else {
            return true;
        }
    }
//@return Se crea funcion para validar la url con regex

    public boolean validarWeb(String camp) {
        Pattern patron = Pattern.compile("<\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]>");
        Matcher mather = patron.matcher(camp);
        if (!mather.find()) {
            return false;
        } else {
            return true;
        }
    }
//@return Se crea fucion para validar si el campo no supere los 140 caracteres

    public boolean validaSize2(String camp) {
        return camp.length() < 140;
    }

    /*
    @@return Se crea una funcion boolean con un string de palabras no apropiadas y luego
    convertirlo en array y buscar la palabra en el comentario introducido
     */
    public boolean validarInsulto(String camp) {
        String paraules = "ximple imbècil babau inútil burro loser noob "
                + "capsigrany torrecollons fatxa nazi supremacista";
        String[] palabras = paraules.split("\\W+");
        for (String palabra : palabras) {
            if (camp.contains(palabra)) {
                return false;
            }
        }
        return true;
    }
}

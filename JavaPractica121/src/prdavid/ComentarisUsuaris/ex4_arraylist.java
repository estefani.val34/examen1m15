/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComentarisUsuaris;

import java.util.ArrayList;
import java.util.List;

/**
 * contains the exercice 4 with arraylists
 *
 * @author tarda
 */
public class ex4_arraylist {

    static List<Usuari> comentaris = new ArrayList<>();

    /**
     * main calls all the functions and contains an arraylist of users
     *
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Usuari> contactList = new ArrayList<Usuari>();
        Usuari user1 = new Usuari("user1", "user1@gmail.com", "www.thisismywebsite1.com");
        Usuari user2 = new Usuari("user2", "user2@gmail.com", "www.thisismywebsite2.com");
        Usuari user3 = new Usuari("user3", "user3@gmail.com", "www.thisismywebsite3.com");
        addComentsUser1(user1);
        addComentsUser2(user2);
        addComentsUser3(user3);
        contactList.add(user1);
        contactList.add(user2);
        contactList.add(user3);

        String name_user = "user2";
        System.out.println("Muestra un comentario aleatorio del user2:");
        for (Usuari usuari : contactList) {
            if (usuari.getNom().equals(name_user)) {
                mostraUnComentari(usuari);
            }
        }
        System.out.println("\nMuestra todos los comentarios:");
        for (Usuari usuari : contactList) {
            mostrarComentaris(usuari);
        }
    }

    /**
     * function addComent1 add coments to user1
     *
     * @param user
     */
    private static void addComentsUser1(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario 1");
        coments.add("Esto es el segundo comentario del usuario 1");
        coments.add("Esto es el tercer comentario del usuario 1");
        coments.add("Esto es el cuarto y ultimo comentario del usuario 1");
        user.setComentari(coments);
    }

    /**
     * function addComent2 add coments to user2
     *
     * @param user
     */
    private static void addComentsUser2(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario 2");
        coments.add("Esto es el segundo comentario del usuario 2");
        coments.add("Esto es el tercer comentario del usuario 2");
        coments.add("Esto es el cuarto y ultimo comentario del usuario 2");
        user.setComentari(coments);
    }

    /**
     * function addComent3 add coments to user3
     *
     * @param user
     */
    private static void addComentsUser3(Usuari user) {
        ArrayList<String> coments = new ArrayList<>();
        coments.add("Esto es el primer comentario del usuario 3");
        coments.add("Esto es el segundo comentario del usuario 3");
        coments.add("Esto es el tercer comentario del usuario 3");
        coments.add("Esto es el cuarto y ultimo comentario del usuario 3");
        user.setComentari(coments);
    }

    /**
     * function mostraUnComentari shows a random comment
     *
     * @param u
     */
    private static void mostraUnComentari(Usuari u) {
        int rand = (int) Math.floor(Math.random() * 4);
        System.out.println(u.getComentari().get(rand));
    }

    /**
     * function mostrarComentaris shows all the commentts from a user
     *
     * @param u
     */
    private static void mostrarComentaris(Usuari u) {
        for (int i = 0; i < u.getComentari().size(); i++) {
            System.out.println(u.getComentari().get(i));
        }
    }

    /**
     * function trobarComentariNomUsuari
     * function that returns all the comments from a user
     * @param nom 
     */
    private static void trobarComentariNomUsuari(String nom) {
        Usuari resultat = new Usuari();
        boolean trobat = false;
        for (int i = 0; i < comentaris.size() - 1 && !trobat; i++) {
            resultat = comentaris.get(i);
            if (resultat.getNom().equals(nom)) {
                trobat = true;
            }
        }
        if (trobat) {
            System.out.println(resultat);
        } else {
            System.out.println("No trobat");
        }
        // return resultat;
    }

}

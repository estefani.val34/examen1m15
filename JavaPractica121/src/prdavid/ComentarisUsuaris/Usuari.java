/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComentarisUsuaris;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class usuari
 * @author tarda
 */
public class Usuari {
    String nom;
    String email;
    String web;
    private ArrayList<String> comentari;

    /**
     * Constructor
     * @param nom
     * @param email
     * @param web
     * @param comentari 
     */
    public Usuari(String nom, String email, String web, ArrayList<String> comentari) {
        this.nom = nom;
        this.email = email;
        this.web = web;
        this.comentari = comentari;
    }
    
    /**
     * Constructor
     * @param nom
     * @param email
     * @param web 
     */
    public Usuari(String nom, String email, String web) {
        this.nom = nom;
        this.email = email;
        this.web = web;
    }
    
    /**
     * Constructor
     */
    public Usuari() {

    }


    /**
     * Function toString (Overrided)
     * @return string
     */
    @Override
    public String toString() {
        return "Comentaris{" + "nom=" + nom + ", email=" + email + ", web=" + web + ", comentari=" + comentari + '}';
    }

    /**
     * function hashCode (Overrided)
     * @return hash
     */
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * function equals (Overrided)
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuari other = (Usuari) obj;
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        return true;
    }
    
    /**
     * getNom
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * setNom
     * @param nom 
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * getEmail
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * setEmail
     * @param email 
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * getWeb
     * @return web
     */
    public String getWeb() {
        return web;
    }

    /**
     * setWeb
     * @param web 
     */
    public void setWeb(String web) {
        this.web = web;
    }
    
    /**
     * getComentari
     * @return arraylist of strings
     */
    public ArrayList<String> getComentari() {
        return comentari;
    }

    /**
     * setComentari
     * @param comentari 
     */
    public void setComentari(ArrayList<String> comentari) {
        this.comentari = comentari;
    }


    
    
    
}

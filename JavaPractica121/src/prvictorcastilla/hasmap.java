/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author tarda
 */
public class hasmap {
    
    private static Map<String,Comentaris> people = new HashMap<String,Comentaris> ();
    
    private static void crearComentaris() {
        people.put("user1",new Comentaris("user1","a.a@a","w.w","hey") );
        people.put("user3",new Comentaris("user3","a.a@a","w.w","eo") );
        people.put("admin1",new Comentaris("admin1","a.a@a","a.a","holi") );
    }
    
    
    private static void mostrarComentaris() {
        for (String nombrePersona: people.keySet()){
            String clave = nombrePersona;
            Comentaris persona = people.get(clave);  
            System.out.println(clave + " " + persona);  
        } 
    }
    
    public static void main(String[] args) {
        crearComentaris();
//        mostrarComentaris();
        String nomATrobar = "admin1";
        System.out.println("Comentaris del " +nomATrobar);
        System.out.println(people.get(nomATrobar));
        nomATrobar="u1";
        System.out.println("Comentaris del " +nomATrobar);
        System.out.println(people.get(nomATrobar));
    }
}
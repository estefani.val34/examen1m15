/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientmanagerap;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alumne
 */
public class ClientManagerAp {
    //private List<Client> myClients = new ArrayList<Client>();
	
	public static void main (String args[]) {
		Menu mainMenu = buildMainMenu();  //main menu of our application.
		boolean exit = false;  //flag to exit application.
		int optionSelected;
		//ClientManagerAp clientManager = new ClientManagerAp();
		
		// load initial data.
		//clientManager.loadClients();
                
                ClientList clientList=new ClientList();
                clientList.loadClients();
		
		do {
			
			mainMenu.show();
			System.out.print("Choose an option: ");
			optionSelected = mainMenu.choose();
			
			switch (optionSelected) {
				case 0: //exit application.
					exit = true;
					break;
				case 1: //find a client by id.
					clientList.findAClientById();
                                       
					break;
				case 2: //list all clients.
					//clientManager.listAllClients();
					break;
				case 3: //add a new client.
					//clientManager.addANewClient();
					break;
				case 4: //modify a client.
					//clientManager.modifyAClient();
					break;
				case 5: //remove a client.
					//clientManager.removeAClient();
					break;
				default:
					System.out.println("Invalid option");
					break;
			}
			
		} while (!exit);
	
	}
	
	/** buildMainMenu()
	 * Builds and returns the main menu of our application.
	 */
	private static Menu buildMainMenu() {
		Menu mnu = new Menu("Client manager application");
		mnu.add( new Option("Exit") );
		mnu.add( new Option("Find a client by id") );
		mnu.add( new Option("List all clients") );
		mnu.add( new Option("Add a new client") );
		mnu.add( new Option("Modify a client") );
		mnu.add( new Option("Remove a client") );
		return mnu;
	}
	


}

package clientmanagerap;

/**
 * Client.java
 * TAD Client.
 * @author José Moreno.
 * @version 2013/10/08.
 */

public class Client {
	/*==== Attributes ====*/
	private String nif;		//nif.
	private String name;	//name or social name.
	private String phone;	//phone.
	private String address;	//mail address.
	private double amount;  //amount of sales.
	
	/*==== Constructors ====*/
	
	public Client() {		
	}
	public Client(String nif, String name, String phone, String address, double amount) {
		this.nif = nif;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.amount = amount;
	}
	
	/*==== Accessors ====*/
	
	public String getNif() {
		return nif;
	}
	public void setNif(String nif) {
		this.nif = nif;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	/*==== Methods ====*/	

	/**
	 * toString()
	 * builds and returns a String representation of the object.
	 * @return a String representation of the object.
	 */	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Client: ");
		sb.append(" nif=");
		sb.append(nif);
		sb.append(", name=");
		sb.append(name);
		sb.append(", phone=");
		sb.append(phone);		
		sb.append(", address=");
		sb.append(address);
		sb.append(", amount=");
		sb.append(amount);		
		return sb.toString();
	}
	/**
	 * equals()
	 * assess equality of Client objects. 
	 * Two clients are considered equals if their nif attributes are equal to each other.
	 * @param Object other: second object to compare to.
	 * @return true if this is equal to other, false otherwise.
	 */
	public boolean equals(final Object other) {	
		boolean b = false;	
		if (other == null) b = false;  //second object is null.		
		else {	//other!=null
			if (this == other) b = true; //the same object.
			else { //not the same object.
				if (other instanceof Client) { //second object is a Client	
					final Client otherClient = (Client) other;
					//assess equality of clients.
					if (this.nif.equals(otherClient.nif)) b = true;
				}
				else b = false; //not a Client.
			}
		}	
		return b;
	}
        //modificar el cliente 
        //modificar el cliente del clienteLIst 
        
	
}


package Ej3_FormValidations;

public class Ej3 {

    private static final String YES = "Yes";
    private static final String NO = "No";

    /**
     * This function prints the answer
     *
     * @param positiveAnswer
     */
    private static void printAnswer(boolean positiveAnswer) {
        if (positiveAnswer) {
            System.out.println(YES);
        } else {
            System.out.println(NO);
        }
    }

    /**
     * This function checks the different validations
     *
     * @param args
     */
    public static void main(String[] args) {
        Validator myValidator = new Validator();
        String name;
        String email;
        String URL;
        String comments;

        //Field name not empty
        System.out.println("Field name empty?");
        name = "Laura";
        System.out.println("Name: " + name);
        printAnswer(!myValidator.valNotEmpty(name));
        name = "";
        System.out.println("Name: " + name);
        printAnswer(!myValidator.valNotEmpty(name));
        name = null;
        System.out.println("Name: " + name);
        printAnswer(!myValidator.valNotEmpty(name));

        //Field email not empty
        System.out.println("\nField email empty?");
        email = "laura@gmail.com";
        System.out.println("Email: " + email);
        printAnswer(!myValidator.valNotEmpty(email));
        email = "";
        System.out.println("Email: " + email);
        printAnswer(!myValidator.valNotEmpty(email));
        email = null;
        System.out.println("Email: " + email);
        printAnswer(!myValidator.valNotEmpty(email));

        //Field comments not empty
        System.out.println("\nField comments empty?");
        comments = "Hola, me llamo Laura";
        System.out.println("Comments: " + comments);
        printAnswer(!myValidator.valNotEmpty(comments));
        comments = "";
        System.out.println("Comments: " + comments);
        printAnswer(!myValidator.valNotEmpty(comments));
        comments = null;
        System.out.println("Comments: " + comments);
        printAnswer(!myValidator.valNotEmpty(comments));

        //Field name first letter uppercase
        System.out.println("\nFirst letter uppercase in field name?");
        name = "Laura";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valFirstLetterUppercase(name));
        name = "laura";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valFirstLetterUppercase(name));
        name = "LAURA";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valFirstLetterUppercase(name));
        name = "lAurA";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valFirstLetterUppercase(name));

        //Field name shorter than or equal to 40 chars
        System.out.println("\nField name shorter than or equal to 40 chars?");
        name = "Laura";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valMaxLength(name, 40));
        name = "Lauraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        System.out.println("Name: " + name);
        printAnswer(myValidator.valMaxLength(name, 40));

        //Field email shorter than or equal to 40 chars
        System.out.println("\nField email shorter than or equal to 40 chars?");
        email = "laura@gmail.com";
        System.out.println("Name: " + email);
        printAnswer(myValidator.valMaxLength(email, 40));
        email = "lauraaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com";
        System.out.println("Name: " + email);
        printAnswer(myValidator.valMaxLength(email, 40));

        //Field URL shorter than or equal to 40 chars
        System.out.println("\nField URL shorter than or equal to 40 chars?");
        URL = "www.laura.com";
        System.out.println("URL: " + URL);
        printAnswer(myValidator.valMaxLength(URL, 40));
        URL = "www.lauraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.com";
        System.out.println("URL: " + URL);
        printAnswer(myValidator.valMaxLength(URL, 40));

        //Field comments shorter than or equal to 140 chars
        System.out.println("\nField comments shorter than or equal to 140 chars?");
        comments = "Hola, me llamo Laura";
        System.out.println("Comments: " + comments);
        printAnswer(myValidator.valMaxLength(comments, 140));

        //Field email valid
        System.out.println("\nField email valid?");
        email = "laura@gmail.com";
        System.out.println("Email: " + email);
        printAnswer(myValidator.valEmail(email));
        email = "laura.gmail.com";
        System.out.println("Email: " + email);
        printAnswer(myValidator.valEmail(email));

        //Field URL valid
        System.out.println("\nField URL valid?");
        URL = "http://www.laura.com/";
        System.out.println("URL: " + URL);
        printAnswer(myValidator.valURL(URL));
        URL = "laura@com";
        System.out.println("URL: " + URL);
        printAnswer(myValidator.valURL(URL));

        //Field comments doesn't contain forbidden words
        System.out.println("\nField comments contains forbidden words?");
        comments = "idiot";
        System.out.println("Comments: " + comments);
        printAnswer(myValidator.valNotForbiddenWords(comments.toLowerCase()));
        comments = "LOSER";
        System.out.println("Comments: " + comments);
        printAnswer(myValidator.valNotForbiddenWords(comments.toLowerCase()));

    }
}
